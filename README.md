# konsole-colorschemes
Collection of .colorscheme files for Konsole (KDE Plasma 5 terminal)

## The convert script
The "convert" script can be used to convert colour schemes from
https://terminal.sexy/

## Location of Konsole .colorscheme files
The .colorscheme files can be placed in
```
~/.local/share/konsole/
```
You may need to re-start Konsole to make these appear.


